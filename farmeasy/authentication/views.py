# from rest_framework.decorators import api_view
from rest_framework import generics
from utils.auth import JWTAuthentication
from utils.exceptions import InvalidInput
from utils.responses import get_success_response
from .serializers import UserSerializer
from .services import (
    is_user_registered, register_user, validate_user_by_phone, generate_and_send_otp, is_otp_valid,
    update_profile
)

# @api_view(['POST'])
# def login(request):
    
class Register(generics.GenericAPIView):
    def post(self, request):
        mobile = request.data['phone']
        is_user_registered(mobile)
        args = request.data
        register_user(**args)
        return get_success_response("You've registred successfully", {})
    

class Login(generics.GenericAPIView):
    def post(self, request):
        user = validate_user_by_phone(request.data['phone'])
        generate_and_send_otp(user)
        return get_success_response("OTP has been sent to your mobile", {})
    

class VerifyOTP(generics.GenericAPIView):
    def post(self, request):
        phone, otp = [request.data[k] for k in ['phone','otp']]
        user = validate_user_by_phone(phone)
        if not is_otp_valid(phone, otp):
                raise InvalidInput("otp is invalid")
        return get_success_response("OTP varified successfully", {"token": user.get_jwt_token(),})


class GetUserDetails(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def get(self, request):
        user = request.user
        serializer = UserSerializer(user)
        data = serializer.data
        return get_success_response(data=data)


class UpdateProfile(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        user = request.user
        args = request.data
        update_profile(user, **args)
        return get_success_response("Your profile has been updated", {})