from rest_framework import serializers
from .models import User 


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            "id",
            "phone",
            "name",
            "email",
            "photo",
            "village",
            "township",
            "pin_code",
            "district",
            "state"
        ]

