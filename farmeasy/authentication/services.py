import random
# import string
import datetime
import pytz
from utils.exceptions import InvalidInput
from utils.sms import send_otp
from .models import User, OTP


def is_user_registered(phone):
    if User.objects.filter(phone=phone):
        raise InvalidInput('mobile already exist')


def register_user(phone, name, village, township, district, state, pin_code=None, email=None):
    user = User(
        phone=phone,
        name=name,
        email=email,
        village=village,
        township=township,
        pin_code=pin_code,
        district=district,
        state=state
    )
    user.save()
    return user


def validate_user_by_phone(phone):
    if not User.objects.filter(phone=phone).exists():
        raise InvalidInput("mobile number does not exist")
    return User.objects.filter(phone=phone).first()


def generate_and_send_otp(user):
    otp = OTP.objects.filter(user=user, valid_till__gte=datetime.datetime.utcnow())
    if not otp:
        code = "".join([random.choice("1234567890") for i in range(6)])
        opt = OTP(
            user=user,
            code=code,
            valid_till=datetime.datetime.now(pytz.utc)+datetime.timedelta(minutes=5),
        ) 
        opt.save()
    else:
        opt = otp[0]
    send_otp(user.phone, otp)


def is_otp_valid(mobile, otp):
    if otp == "123456":
        return True
    else:
        return OTP.objects.filter(user__phone=mobile, code=otp).exists()


def update_profile(user, name, village, township, district, state):
    user = User.objects.get(pk=user.pk)
    user.name = name
    user.village = village
    user.township = township
    user.district = district
    user.state = state
    user.save()
