from django.conf.urls import url
# from django.urls import path
from . import views

urlpatterns = [
    url(r'^register', views.Register.as_view()),
    url(r'^login', views.Login.as_view()),
    url(r'^verify_otp', views.VerifyOTP.as_view()),
    url(r'^get_user_details', views.GetUserDetails.as_view()),
    url(r'^update_user_profile', views.UpdateProfile.as_view()),
]

# urlpatterns = [
#     path('', views.index, name='index'),
# ]