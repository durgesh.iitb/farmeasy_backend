import jwt
from django.db import models
from django.conf import settings
from utils.abstract_models import Base


# Create your models here.
class User(Base):
    phone = models.CharField(max_length=25)
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=50, blank=True, null=True)
    photo = models.ImageField(upload_to="media/", null=True, blank=True)
    village = models.CharField(max_length=250)
    township = models.CharField(max_length=250)
    pin_code = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250)
    state = models.CharField(max_length=250)

    def __str__(self):
        return f"{self.phone} - {self.name}"

    def get_jwt_token(self):
        payload = {"user_id": self.pk, "user_name": "doctor"}
        token = jwt.encode(payload, settings.SECRET_KEY, algorithm="HS256")
        return token
    
    @classmethod
    def from_token(cls, token):
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        return cls.objects.get(pk=payload["user_id"])


class OTP(Base):
    user = models.ForeignKey("User", on_delete=models.CASCADE)
    code = models.CharField(max_length=6)
    valid_till = models.DateTimeField()

    def __str__(self):
        return "{} - {}".format(self.user.phone, self.code)
