from rest_framework.response import Response


def get_success_response(message="", data=None):
    return Response({
        "status": {
            "result": 1,
            "message": message
        },
        "data": data
    })


def get_raw_response(data=None):
    return Response(data)
