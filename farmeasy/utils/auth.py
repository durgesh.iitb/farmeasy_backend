from rest_framework import authentication
from rest_framework import permissions
from authentication.models import User
from utils.handler import InvalidToken


class JWTAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.headers.get('Authorization')
        if not token:  # no token passed in request headers
            raise InvalidToken('Token not provided')
        try:
            user = User.from_token(token)  # get the user
            print("got a user")
        except Exception:
            raise InvalidToken('Your session has expired, please login again')
        # request.today = datetime.datetime.today().astimezone(pytz.timezone('Asia/Kolkata')).date()
        return (user, None)  # authentication successfull


class BasicPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        # if isinstance(request.user, Doctor):
        #     request.user.set_active()
        return isinstance(request.user, User)
