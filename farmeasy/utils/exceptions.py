from rest_framework import status
from utils.handler import FarmEasyException


class InvalidInput(FarmEasyException):
    def __init__(self, message):
        self.message = message
        self.http_code = status.HTTP_200_OK