from rest_framework.response import Response
from rest_framework import status


def custom_exception_handler(exc, context):
    if not isinstance(exc, FarmEasyException):
        raise(exc)
        response = Response({
            "status": {
                "result": 0,
                "message": "An internal server error occured"
            }
        }, status=status.HTTP_200_OK)
    else:
        response = Response({
            "status": {
                "result": 0,
                "message": exc.message
            }
        }, status=exc.http_code)
    return response


class FarmEasyException(Exception):
    pass


class InvalidToken(FarmEasyException):
    def __init__(self, message):
        self.message = message
        self.http_code = status.HTTP_401_UNAUTHORIZED
