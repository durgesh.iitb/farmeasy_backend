from pytz import timezone
from django.db import models


class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    @property
    def ist_date(self):
        return self.created_at.astimezone(timezone("Asia/Kolkata"))

    @property
    def readable_time(self):
        return self.created_at.astimezone(timezone("Asia/Kolkata")).strftime(
            "%d %b, %Y %I:%M %p"
        )

    @property
    def readable_time_only(self):
        return self.created_at.astimezone(timezone("Asia/Kolkata")).strftime(
            "%I:%M %p"
        )
