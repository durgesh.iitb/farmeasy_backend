# import random
# import datetime
# import pytz
from utils.exceptions import InvalidInput
from authentication.models import User
from .models import SellLead, SellLeadImage, BuyLead


def create_sell_lead(user, crop_name, quantity, description, images):
    lead = SellLead(
        user=user,
        crop_name=crop_name,
        quantity=quantity,
        description=description
    )
    lead.save()

    # images[0].seek
    # img_urls = []
    for image in images:
        SellLeadImage.objects.create(lead=lead, image=image)


def get_all_sell_leads():
    leads = SellLead.objects.all().order_by(
        "-created_at"
    )
    return leads


def get_sell_lead_data(id):
    if not SellLead.objects.filter(id=id).exists():
        raise InvalidInput("lead id does not exist")
    lead = SellLead.objects.get(id=id)
    return lead


def get_sell_leads_for_user(user):
    leads = SellLead.objects.filter(user=user).order_by(
        "-created_at"
    )
    return leads


def create_buy_lead(user, crop_name, quantity, description):
    lead = BuyLead(
        user=user,
        crop_name=crop_name,
        quantity=quantity,
        description=description
    )
    lead.save()


def get_all_buy_leads():
    leads = BuyLead.objects.all().order_by(
        "-created_at"
    )
    return leads


def get_buy_leads_for_user(user):
    leads = BuyLead.objects.filter(user=user).order_by(
        "-created_at"
    )
    return leads


def get_buy_lead_data(id):
    if not BuyLead.objects.filter(id=id).exists():
        raise InvalidInput("lead id does not exist")
    lead = BuyLead.objects.get(id=id)
    return lead


def delete_user_lead(user, lead_id, lead_type):
    if lead_type == 'sell':
        SellLead.objects.filter(user=user, id=lead_id).delete()
    else:
        BuyLead.objects.filter(user=user, id=lead_id).delete()
