from django.contrib import admin
from .models import SellLead, SellLeadImage, BuyLead
# Register your models here.

admin.site.register(SellLead)
admin.site.register(SellLeadImage)
admin.site.register(BuyLead)
