from rest_framework import serializers
from authentication.serializers import UserSerializer
from .models import SellLead, BuyLead, SellLeadImage


class SellLeadSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = SellLead
        fields = [
            "id",
            "crop_name",
            "quantity",
            "description",
            "lead_type",
            "readable_time",
            "ist_date",
            "user"
        ]


class SellLeadImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = SellLeadImage
        fields = [
            "id",
            "image",
            "ist_date",
            "get_image_url"
        ]


class SellLeadDetailsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    images = SellLeadImageSerializer(many=True)

    class Meta:
        model = SellLead
        fields = [
            "id",
            "crop_name",
            "quantity",
            "description",
            "lead_type",
            "readable_time",
            "ist_date",
            "user",
            "images"
        ]


class BuyLeadSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = BuyLead
        fields = [
            "id",
            "crop_name",
            "quantity",
            "description",
            "lead_type",
            "readable_time",
            "ist_date",
            "user"
        ]
