from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^create_sell_lead', views.CreateSellLead.as_view()),
    url(r'^get_all_sell_leads', views.GetAllSellLeads.as_view()),
    url(r'^get_user_sell_leads', views.GetUserSellLeads.as_view()),
    url(r'^sell_lead_details', views.SellLeadDetails.as_view()),
    url(r'^create_buy_lead', views.CreateBuyLead.as_view()),
    url(r'^get_all_buy_leads', views.GetAllBuyLeads.as_view()),
    url(r'^get_user_buy_leads', views.GetUserBuyLeads.as_view()),
    url(r'^buy_lead_details', views.BuyLeadDetails.as_view()),
    url(r'^get_all_user_leads', views.GetAllUserLeads.as_view()),
    url(r'^delete_user_lead', views.DeleteUserLead.as_view()),
]
