# from rest_framework.decorators import api_view
from rest_framework import generics
from utils.auth import JWTAuthentication
from utils.exceptions import InvalidInput
from utils.responses import get_success_response
from .services import (
    create_sell_lead, get_all_sell_leads, get_sell_leads_for_user, get_sell_lead_data, 
    create_buy_lead, get_all_buy_leads, get_buy_leads_for_user, get_buy_lead_data, delete_user_lead
)
from .serializers import SellLeadSerializer, SellLeadDetailsSerializer, BuyLeadSerializer
# @api_view(['POST'])
# def login(request):

class CreateSellLead(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        user = request.user
        crop_name, quantity, description = [
            request.data[k] for k in ["crop_name", 'quantity', "description"]
        ]
        images = request.FILES.getlist("images") if request.FILES.getlist("images") else [] 
        create_sell_lead(user, crop_name, quantity, description, images)
        return get_success_response("Your lead has been created", {})


class GetAllSellLeads(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def get(self, request):
        leads = get_all_sell_leads()
        serializer = SellLeadSerializer(leads, many=True)
        return get_success_response(
            "", {'sell_leads':serializer.data}
        )


class GetUserSellLeads(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def get(self, request):
        user = request.user
        leads = get_sell_leads_for_user(user)
        serializer = SellLeadSerializer(leads, many=True)
        return get_success_response(
            "", {'user_sell_leads':serializer.data}
        )


class SellLeadDetails(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        lead = get_sell_lead_data(request.data["id"])
        serializer = SellLeadDetailsSerializer(lead)
        return get_success_response(
            "", {'lead_data':serializer.data}
        )


class CreateBuyLead(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def post(self, request):
        user = request.user
        args = request.data
        create_buy_lead(user, **args)
        return get_success_response("Your lead has been created", {})


class GetAllBuyLeads(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def get(self, request):
        leads = get_all_buy_leads()
        serializer = BuyLeadSerializer(leads, many=True)
        return get_success_response(
            "", {'buy_leads':serializer.data}
        )


class GetUserBuyLeads(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def get(self, request):
        user = request.user
        leads = get_buy_leads_for_user(user)
        serializer = BuyLeadSerializer(leads, many=True)
        return get_success_response(
            "", {'user_buy_leads':serializer.data}
        )


class BuyLeadDetails(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)
    
    def post(self, request):
        lead = get_buy_lead_data(request.data["id"])
        serializer = BuyLeadSerializer(lead)
        return get_success_response(
            "", {'lead_data':serializer.data}
        )


class GetAllUserLeads(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)

    def get(self, request):
        user = request.user
        sell_leads = get_sell_leads_for_user(user)
        sell_lead_serializer = SellLeadSerializer(sell_leads, many=True)
        sell_leads_data = sell_lead_serializer.data
        buy_leads = get_buy_leads_for_user(user)
        buy_lead_serializer = BuyLeadSerializer(buy_leads, many=True)
        buy_leads_data = buy_lead_serializer.data
        all_leads = buy_leads_data + sell_leads_data
        return get_success_response(
            "", {'user_leads':all_leads}
        )


class DeleteUserLead(generics.GenericAPIView):
    authentication_classes = (JWTAuthentication,)
    
    def post(self, request):
        user = request.user
        lead_id, lead_type = [request.data[k] for k in ['id', 'lead_type']]
        delete_user_lead(user, lead_id, lead_type)
        return get_success_response(
            "Your lead has been deleted", {}
        )
