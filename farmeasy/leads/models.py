from django.db import models
from utils.abstract_models import Base

# Create your models here.

class SellLead(Base):
    user = models.ForeignKey("authentication.User", on_delete=models.CASCADE)
    crop_name = models.CharField(max_length=250)
    quantity = models.CharField(max_length=250)
    description = models.TextField()
    lead_type = models.CharField(max_length=10, null=True, blank=True, default="sell")

    def __str__(self):
        return f"{self.user.phone} - {self.user.name} - {self.crop_name}"


class SellLeadImage(Base):
    lead = models.ForeignKey(
        "SellLead", on_delete=models.CASCADE, related_name="images"
    )
    image = models.ImageField(upload_to="images/", null=True, blank=True)

    @property
    def get_image_url(self):
        return self.image.url.split("?")[0]


class BuyLead(Base):
    user = models.ForeignKey("authentication.User", on_delete=models.CASCADE)
    crop_name = models.CharField(max_length=250)
    quantity = models.CharField(max_length=250)
    description = models.TextField()
    lead_type = models.CharField(max_length=10, null=True, blank=True, default="buy")

    def __str__(self):
        return f"{self.user.phone} - {self.user.name} - {self.crop_name}"
