# Generated by Django 3.1.7 on 2021-02-26 10:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0002_otp'),
        ('leads', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buylead',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authentication.user'),
        ),
        migrations.AlterField(
            model_name='selllead',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authentication.user'),
        ),
        migrations.AlterField(
            model_name='sellleadimage',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='media/images'),
        ),
    ]
